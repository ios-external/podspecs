Pod::Spec.new do |s|
  s.name                  = 'Perfios+Epan'
  s.version               = '1.0.5'
  s.summary               = 'Epan library by Perfios Software Solutions Private Ltd.'
  s.description           = <<-DESC
  Perfios+Epan is a library developed by Perfios Software Solutions Private Ltd. to support Epan Verification.
    DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_epan_framework_perfios.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "10.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_epan_framework_perfios.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "Epan.xcframework"
  s.dependency "Perfios+Lottie"
end