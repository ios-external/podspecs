Pod::Spec.new do |s|
  s.name                  = 'Karza+Liveness'
  s.version               = '0.2.2'
  s.summary               = 'Silent liveness verification Framework by Karza Technologies'
  s.description           = <<-DESC
    Karza+Liveness SDK is utilized to testify the liveness of a person by analyzing image samples of the customer's face . We use silent or passive liveness detection since it is non-intrusive and is challenging for the user to know if he/she is being validated for liveness or not.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_liveness_ml_framework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform              = :ios, "11.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_liveness_ml_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "Liveness.xcframework"
end
