Pod::Spec.new do |s|
  s.name                = 'AadhaarQRReader'
  s.version             = '0.1.1'
  s.summary             = 'An SDK to read QR code present on Aadhaar Card'
  s.description         = <<-DESC
AadhaarQRReader is a library developed by Karza Technologies. This pod helps iOS apps to read the data present in the QR code of the Aadhaar card.
It can read the QR using the camera and also by uploading a PDF file of the Aadhaar card.
It can read both the XML based and the new Aadhaar secure QR.
DESC
  s.homepage            = 'https://bitbucket.org/ios-external/ios_aadhaar_qr_framework'
  s.license             = { :type => "APACHE", :file => "FILE_LICENSE" }
  s.author              = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform            = :ios, "10.0"
  s.source              = { :git => 'https://bitbucket.org/ios-external/ios_aadhaar_qr_framework.git', :tag => "#{s.version}" }
  s.exclude_files       = "Classes/Exclude"
  s.public_header_files = "AadhaarQRReader.framework/Headers/*.h"
  s.source_files        = "AadhaarQRReader.framework/Headers/*.h"
  s.vendored_frameworks = "AadhaarQRReader.framework"
  s.dependency "Karza+BigInt"
  s.dependency "Karza+Gzip"
end
