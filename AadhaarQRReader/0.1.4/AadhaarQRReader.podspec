Pod::Spec.new do |s|
  s.name                  = 'AadhaarQRReader'
  s.version               = '0.1.4'
  s.summary               = 'An SDK to read QR code present on Aadhaar Card'
  s.description           = <<-DESC
    AadhaarQRReader is a library developed by Karza Technologies. This pod helps iOS apps to read the data present in the QR code of the Aadhaar card.
    It can read the QR using the camera and also by uploading a PDF file of the Aadhaar card.
    It can read both the XML based and the new Aadhaar secure QR.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_aadhaar_qr_framework'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform              = :ios, "10.0"
  s.pod_target_xcconfig   = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig  = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_aadhaar_qr_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "AadhaarQRReader.xcframework"
  s.dependency "Karza+BigInt"
  s.dependency "Karza+Gzip"
end
