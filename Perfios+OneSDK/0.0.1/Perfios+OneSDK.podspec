Pod::Spec.new do |s|
  s.name                  = 'Perfios+OneSDK'
  s.version               = '0.0.1'
  s.summary               = 'OneSDK Framework by Perfios Software Solution Pvt Ltd'
  s.description           = <<-DESC
    Perfios+OneSDK SDK is utilized to create seamless KYC journeys for customer.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_onesdk_framework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "16.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_onesdk_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "OneSDK.xcframework"

  s.dependency 'DivKit', '29.14.0'
  s.dependency 'DivKitExtensions', '29.14.0'
  s.dependency 'Perfios+Liveness'
  s.dependency 'Perfios+AadhaarXML'
  s.dependency 'Perfios+OCR'
  s.dependency 'Perfios+Digilocker'
  s.dependency 'Perfios+AadhaarQR'
end
