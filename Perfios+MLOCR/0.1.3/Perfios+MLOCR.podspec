Pod::Spec.new do |s|
  s.name                = 'Perfios+MLOCR'
  s.version             = '0.1.3'
  s.summary             = 'An SDK to detect document type in the provided image'
  s.description         = <<-DESC
  Perfios+MLOCR is a library developed by Perfios Software Solutions Private Ltd. This pod helps iOS apps to detect the document provided to the SDK.It can detect document type, its coordinates, determine whether the image is blur, also determine brightness level.
DESC
  s.homepage            = 'https://bitbucket.org/ios-external/ios_ocr_ml_framework_perfios.git'
  s.license             = { :type => "MIT", :file => "LICENSE" }
  s.author              = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform            = :ios, "11.0"
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' } # Reason TensorFlow don't have arm64 for Simulator
  s.source              = { :git => 'https://iOSPublicPods@bitbucket.org/ios-external/ios_ocr_ml_framework_perfios.git', :tag => "#{s.version}" }
  s.vendored_frameworks = "ios_ocr_ml.xcframework"
  s.dependency "Perfios+GEOSwift"
  s.dependency "Perfios+CropViewController"
end
