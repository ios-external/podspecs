Pod::Spec.new do |s|
  s.name                  = 'Perfios+UnifiedKYC'
  s.version               = '0.2.7'
  s.summary               = 'UnifiedKYC Framework by Perfios Software Solutions Private Ltd'
  s.description           = <<-DESC
  UnifiedKYC is a library developed by Perfios Software Solutions Private Ltd to support UnifiedKYC verification.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_unified_kyc_framework_perfios.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "11.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_unified_kyc_framework_perfios.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "Unifiedkyc.xcframework"
  s.dependency 'Perfios+OCR'
  s.dependency 'Perfios+Liveness'
  s.dependency 'Perfios+Digilocker'
  s.dependency 'Perfios+AadhaarQR'
  s.dependency 'Perfios+AadhaarXML'
end
