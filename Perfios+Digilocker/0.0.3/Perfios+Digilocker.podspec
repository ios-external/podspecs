Pod::Spec.new do |s|
  s.name                  = 'Perfios+Digilocker'
  s.version               = '0.0.3'
  s.summary               = 'Digilocker library by Perfios Software Solutions Private Ltd.'
  s.description           = <<-DESC
  Perfios+Digilocker is a library developed by Perfios Software Solutions Private Ltd to support Digilocker Verification.
    DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_digilocker_framework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "10.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_digilocker_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "Digilocker.xcframework"
end