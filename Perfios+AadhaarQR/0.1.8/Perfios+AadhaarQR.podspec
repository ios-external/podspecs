Pod::Spec.new do |s|
  s.name                  = 'Perfios+AadhaarQR'
  s.version               = '0.1.8'
  s.summary               = 'An SDK to read QR code present on Aadhaar Card'
  s.description           = <<-DESC
  Perfios+AadhaarQR is a library developed by Perfios Software Solutions Private Ltd.. This pod helps iOS apps to read the data present in the QR code of the Aadhaar card.
    It can read the QR using the camera and also by uploading a PDF file of the Aadhaar card.
    It can read both the XML based and the new Aadhaar secure QR.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_aadhaar_qr_framework_perfios.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Bhawanisingh Rao' => 'bhawanisingh.r@perfios.com' }
  s.platform              = :ios, "12.0"
  s.pod_target_xcconfig   = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig  = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_aadhaar_qr_framework_perfios.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "AadhaarQR.xcframework"
  s.dependency "Perfios+BigInt"
  s.dependency "Perfios+Gzip"
end
