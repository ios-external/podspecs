Pod::Spec.new do |s|
  s.name                  = 'Karza+OKYC'
  s.version               = '0.2.4'
  s.summary               = 'OKYC Framework by Karza Technologies'
  s.description           = <<-DESC
    OKYC is a library developed by Karza Technologies to support OKYC verification.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_okyc_framework'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform              = :ios, "11.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_okyc_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "Okyc.xcframework"
  s.dependency 'Karza+OCR'
  s.dependency 'Karza+Liveness'
  s.dependency 'Karza+Digilocker'
  s.dependency 'AadhaarQRReader'
  s.dependency 'OfflineAadhaar'
end
