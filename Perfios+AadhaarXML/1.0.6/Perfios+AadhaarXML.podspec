Pod::Spec.new do |s|
  s.name                  = 'Perfios+AadhaarXML'
  s.version               = '1.0.6'
  s.summary               = 'Aadhaar XML verification library by Perfios Software Solutions Private Ltd.'
  s.description           = <<-DESC
  Perfios AadhaarXML is a library developed by Perfios Software Solutions Private Ltd to support Aadhaar XML Verification.
    DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_aadhaar_xml_framework_perfios.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "10.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_aadhaar_xml_framework_perfios.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "OfflineAadhaar.xcframework"
end