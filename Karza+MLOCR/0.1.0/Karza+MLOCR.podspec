Pod::Spec.new do |s|
  s.name                = 'Karza+MLOCR'
  s.version             = '0.1.0'
  s.summary             = 'An SDK to detect document type in the provided image'
  s.description         = <<-DESC
Karza+MLOCR is a library developed by Karza Technologies. This pod helps iOS apps to detect the document provided to the SDK.
It can detect document type, its coordinates, determine whether the image is blur, also determine brightness level.
DESC
  s.homepage            = 'https://bitbucket.org/ios-external/ios_ocr_ml_framework'
  s.license             = { :type => "MIT", :file => "LICENSE" } #PENDING
  s.author              = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform            = :ios, "11.0"
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  # s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.source              = { :git => 'https://bitbucket.org/ios-external/ios_ocr_ml_framework.git', :tag => "#{s.version}" }
  s.exclude_files       = "Classes/Exclude"

  # s.public_header_files = "AadhaarQRReader.framework/Headers/*.h"
  # s.source_files        = "AadhaarQRReader.framework/Headers/*.h"
  s.vendored_frameworks = "AadhaarQRReader.framework"

  s.dependency "GEOSwift"
  s.dependency "Karza+CropViewController"
end
