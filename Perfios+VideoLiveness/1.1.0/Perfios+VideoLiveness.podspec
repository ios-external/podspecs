Pod::Spec.new do |s|
  s.name                  = 'Perfios+VideoLiveness'
  s.version               = '1.1.0'
  s.summary               = 'Video liveness verification Framework by Perfios Software Solution Pvt Ltd'
  s.description           = <<-DESC
    Perfios+VideoLiveness SDK is utilized to testify the liveness of a person by analyzing Video of the customer.
  DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_videoliveness_framework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "12.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_videoliveness_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "VideoLiveness.xcframework"
end
