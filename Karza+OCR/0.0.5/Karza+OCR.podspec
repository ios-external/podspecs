Pod::Spec.new do |s|
  s.name                  = 'Karza+OCR'
  s.version               = '0.0.5'
  s.summary               = 'An SDK to OCR goverment provided IDs'
  s.description           = <<-DESC
    Karza+OkycOCR is a library developed by Karza Technologies. This pod helps iOS apps to OCR goverment provided IDs.
    It can OCR documents like PAN Card, Aadhaar Card, Passport, Driving license, VoterId etc.
  DESC
  s.homepage              = 'https://iOSPublicPods@bitbucket.org/ios-external/ocrframework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform              = :ios, "10.0"
  s.source                = { :git => 'https://iOSPublicPods@bitbucket.org/ios-external/ocrframework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "OkycOCR.xcframework"
  s.dependency "Karza+CropViewController"
end
