Pod::Spec.new do |s|
  s.name                  = 'OfflineAadhaar'
  s.version               = '1.0.6'
  s.summary               = 'Aadhaar XML verification library by Karza Technologies'
  s.description           = <<-DESC
  Offline Aadhaar is a library developed by Karza Technologies to support Aadhaar XML Verification.
    DESC
  s.homepage              = 'https://bitbucket.org/ios-external/offlineaadhaarframework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform              = :ios, "10.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/offlineaadhaarframework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "OfflineAadhaar.xcframework"
end