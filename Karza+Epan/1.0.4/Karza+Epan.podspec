Pod::Spec.new do |s|
  s.name                  = 'Karza+Epan'
  s.version               = '1.0.4'
  s.summary               = 'Epan library by Karza Technologies'
  s.description           = <<-DESC
  Karza+Epan is a library developed by Karza Technologies to support Epan Verification.
    DESC
  s.homepage              = 'https://bitbucket.org/ios-external/ios_epan_framework.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@karza.in' }
  s.platform              = :ios, "10.0"
  s.source                = { :git => 'https://bitbucket.org/ios-external/ios_epan_framework.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "Epan.xcframework"
end