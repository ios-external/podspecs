Pod::Spec.new do |s|
  s.name                  = 'Perfios+OCR'
  s.version               = '0.0.16'
  s.summary               = 'An SDK to OCR goverment provided IDs'
  s.description           = <<-DESC
  Perfios+OCR is a library developed by Perfios Software Solutions Private Ltd. This pod helps iOS apps to OCR goverment provided IDs.
    It can OCR documents like PAN Card, Aadhaar Card, Passport, Driving license, VoterId etc.
  DESC
  s.homepage              = 'https://iOSPublicPods@bitbucket.org/ios-external/ios_ocr_framework_perfios.git'
  s.license               = { :type => "MIT", :file => "LICENSE" }
  s.author                = { 'Sanjay Kumawat' => 'sanjay.k@perfios.com' }
  s.platform              = :ios, "11.0"
  s.source                = { :git => 'https://iOSPublicPods@bitbucket.org/ios-external/ios_ocr_framework_perfios.git', :tag => "#{s.version}" }
  s.vendored_frameworks   = "OCR.xcframework"
  s.dependency              "Perfios+CropViewController"
end
